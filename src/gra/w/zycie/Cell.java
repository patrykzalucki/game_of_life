/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gra.w.zycie;

/**
 *
 * @author Patryk
 */

public class Cell {

    boolean condition = false;
    short r,g,b;
    
    public void setCondition(boolean b)
    {
        condition = b;
    }
    
    public void negationOfCondition()
    {
        condition = !condition;
    }
    public boolean checkCondition()
    {
        return condition;
    }
}
