/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gra.w.zycie;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;
/**
 *
 * @author Patryk
 */
public class Game extends javax.swing.JFrame {

    int width = 60, height = 50; 
    
//    boolean [][] currentSpace,nextSpace;
    Cell [][] currentSpace,nextSpace;
//    boolean [] conditions = new boolean [8];
    
    
    
    boolean mainLoop = false;
    boolean periodic = false;
    Image offScrImg;
    Graphics offScrGraph;

    public Game() {
        width+=2;
        height+=2;
//        currentSpace = new boolean [width][height] ;
//        nextSpace = new boolean [width][height];

        
        currentSpace = new Cell [width][height];
        nextSpace = new Cell [width][height];
        
        clearCells(currentSpace);
        clearCells(nextSpace);
        

        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initComponents();
        offScrImg = createImage (jPanelBoard.getWidth(),jPanelBoard.getHeight());
        offScrGraph = offScrImg.getGraphics();
        
        
        
        Timer time = new Timer();
        paintAgain();
        TimerTask task = new TimerTask(){
            
            public void run()
            {
                boolean b;
                if (mainLoop)
                {
                    if (periodic)
                        periodicBoundaries();
                    
                    
                    
                    for (int i = 1; i<width-1; i++)
                    {
                        for (int j = 1; j<height-1; j++)
                        {
                            nextSpace[i][j].condition = gameOfLifeConditions(i, j);
//                              nextSpace[i][j] = noPeriodicConditions(i, j);
                        }
                    }
                    
                    for (int i = 1; i<width-1; i++)
                    {
                        for (int j = 1; j<height-1; j++)
                        {
                            currentSpace[i][j].condition = nextSpace[i][j].condition;
                        }
                    }
                    paintAgain();
                }
            }
        };
        time.scheduleAtFixedRate(task, 0, 100);
        

    } 
    
    private void clearCells (Cell [][] tab)
    {
        for (int i = 0;i<width;i++)
        {
            for (int j = 0; j<height; j++)
            {
                tab[i][j] = new Cell();
            }
        }
    }
    
    void periodicBoundaries()
    {   
        for(int j = 1; j < height-1; j++)
            currentSpace[0][j]  = currentSpace[width-2][j] ;
        
        for(int j = 1; j < height-1; j++)
            currentSpace[width-1][j]  = currentSpace[1][j] ;
        
        for(int i = 1; i < width-1; i++)
            currentSpace[i][0]  = currentSpace[i][height-2] ;
        
        for(int i = 1; i < width-1; i++)
            currentSpace[i][height-1]  = currentSpace[i][1] ;
        
        currentSpace[0][0]  = currentSpace[width-2][height-2] ;
        currentSpace[width-1][height-1]  = currentSpace[1][1] ;
        currentSpace[width-1][0]  = currentSpace[1][height-2] ;
        currentSpace[0][height-1]  = currentSpace[width-2][1] ;
    }
    
    private boolean gameOfLifeConditions(int i, int j)
    {
        int neightbours = 0;
        
          //Nie mam pojecia dlaczego to nie dziala

          
        if (currentSpace[i-1][j-1].condition) neightbours++;
        if (currentSpace[i][j-1].condition) neightbours++;
        if (currentSpace[i+1][j-1].condition) neightbours++;
        if (currentSpace[i+1][j].condition) neightbours++;
        if (currentSpace[i+1][j+1].condition) neightbours++;
        if (currentSpace[i][j+1].condition) neightbours++;
        if (currentSpace[i-1][j+1].condition) neightbours++;
        if (currentSpace[i-1][j].condition) neightbours++;
        
//        if (currentSpace[i-1][j-1]) neightbours++;
//        if (currentSpace[i][j-1]) neightbours++;
//        if (currentSpace[i+1][j-1]) neightbours++;
//        if (currentSpace[i+1][j]) neightbours++;
//        if (currentSpace[i+1][j+1]) neightbours++;
//        if (currentSpace[i][j+1]) neightbours++;
//        if (currentSpace[i-1][j+1]) neightbours++;
//        if (currentSpace[i-1][j]) neightbours++;

//        if (neightbours>0)
//        System.out.println("Komórka " + i + " "+ j + "ma tylu sąsiadów: " + neightbours);
        
        if (neightbours == 3)
            return true;
        
        if (neightbours < 2 || neightbours > 3)
            return false; 
        
        return currentSpace[i][j].condition;
    }
    
    private void paintAgain(){
        offScrGraph.setColor(jPanelBoard.getBackground());
        offScrGraph.fillRect(0, 0, jPanelBoard.getWidth(),jPanelBoard.getHeight());
        int x,y;
        for (int i = 1; i< width-1; i++)
        {
            for ( int j = 1; j < height-1; j++)
            {       
                    if (currentSpace[i][j].condition)
                    {
                        offScrGraph.setColor(Color.blue);
                        x = (i)* jPanelBoard.getWidth()/width;
                        y = (j)* jPanelBoard.getHeight()/height;
                        offScrGraph.fillRect(x, y, jPanelBoard.getWidth()/width+1,jPanelBoard.getHeight()/height + 1);
                    }
            }
        }
        
        offScrGraph.setColor(Color.black);
        
        
        
        for (int i = 1; i<height; i++)
        {
            y = i* jPanelBoard.getHeight()/height;
            offScrGraph.drawLine(0,y,jPanelBoard.getWidth(),y);    
        }
        for (int i = 1; i<width; i++)
        {
            x = i* jPanelBoard.getWidth()/width;
            offScrGraph.drawLine(x,0,x,jPanelBoard.getHeight());        
        }
        
        offScrGraph.setColor(new Color(238,238,238));
        
        offScrGraph.fillRect(0, 0, jPanelBoard.getWidth(), jPanelBoard.getHeight()/height);
        offScrGraph.fillRect(0,0,jPanelBoard.getHeight()/height,jPanelBoard.getWidth()); 
        offScrGraph.fillRect(0, jPanelBoard.getHeight()-(jPanelBoard.getHeight()/height), jPanelBoard.getWidth(), jPanelBoard.getHeight()/height);
        offScrGraph.fillRect(jPanelBoard.getWidth()-(jPanelBoard.getWidth()/width),0,jPanelBoard.getHeight()/height,jPanelBoard.getWidth()); 
        
        
        jPanelBoard.getGraphics().drawImage(offScrImg,0,0,jPanelBoard);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelPropeties = new javax.swing.JPanel();
        jButtonStart = new javax.swing.JButton();
        jButtonReset = new javax.swing.JButton();
        jButtonRozrzuc = new javax.swing.JButton();
        jSlider1 = new javax.swing.JSlider();
        jLabel1 = new javax.swing.JLabel();
        jButtonPeriodic = new javax.swing.JButton();
        jPanelBoard = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(800, 500));

        jButtonStart.setText("Start");
        jButtonStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStartActionPerformed(evt);
            }
        });

        jButtonReset.setText("Reset");
        jButtonReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonResetActionPerformed(evt);
            }
        });

        jButtonRozrzuc.setText("Rozrzuć losowo");
        jButtonRozrzuc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRozrzucActionPerformed(evt);
            }
        });

        jSlider1.setValue(10);

        jLabel1.setText("Procent żywych komórek");

        jButtonPeriodic.setText("Periodyczność");
        jButtonPeriodic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPeriodicActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelPropetiesLayout = new javax.swing.GroupLayout(jPanelPropeties);
        jPanelPropeties.setLayout(jPanelPropetiesLayout);
        jPanelPropetiesLayout.setHorizontalGroup(
            jPanelPropetiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPropetiesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelPropetiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonRozrzuc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelPropetiesLayout.createSequentialGroup()
                        .addGroup(jPanelPropetiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelPropetiesLayout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addComponent(jLabel1))
                            .addComponent(jButtonStart, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonReset, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonPeriodic))
                        .addGap(0, 5, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelPropetiesLayout.setVerticalGroup(
            jPanelPropetiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPropetiesLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonRozrzuc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 219, Short.MAX_VALUE)
                .addComponent(jButtonStart)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonReset)
                .addGap(50, 50, 50)
                .addComponent(jButtonPeriodic)
                .addContainerGap())
        );

        jPanelBoard.setBackground(new java.awt.Color(255, 255, 255));
        jPanelBoard.setMaximumSize(new java.awt.Dimension(800, 800));
        jPanelBoard.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanelBoardMouseClicked(evt);
            }
        });
        jPanelBoard.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                jPanelBoardComponentResized(evt);
            }
        });

        javax.swing.GroupLayout jPanelBoardLayout = new javax.swing.GroupLayout(jPanelBoard);
        jPanelBoard.setLayout(jPanelBoardLayout);
        jPanelBoardLayout.setHorizontalGroup(
            jPanelBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
        );
        jPanelBoardLayout.setVerticalGroup(
            jPanelBoardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanelPropeties, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelBoard, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPropeties, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanelBoard, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonStartActionPerformed
        mainLoop =! mainLoop;
        if (mainLoop) 
            jButtonStart.setText("Pauza");
        else
            jButtonStart.setText("Start");
        
        paintAgain();
    }//GEN-LAST:event_jButtonStartActionPerformed

    private void jPanelBoardMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelBoardMouseClicked
        
        int i = width * evt.getX()/jPanelBoard.getWidth();
        int j = height * evt.getY()/jPanelBoard.getHeight();
        if((i>0)&&(i<width-1)&&(j>0)&&(j<height-1)){
              currentSpace[i][j].condition =!  currentSpace[i][j].condition; 
        }
        

        paintAgain();
    }//GEN-LAST:event_jPanelBoardMouseClicked

    private void jPanelBoardComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jPanelBoardComponentResized
        offScrImg = createImage (jPanelBoard.getWidth(),jPanelBoard.getHeight());
        offScrGraph = offScrImg.getGraphics();
        paintAgain();
    }//GEN-LAST:event_jPanelBoardComponentResized

    private void jButtonResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonResetActionPerformed
        clearCells(currentSpace);
//        currentSpace = new boolean [width][height] ;
        paintAgain(); 
    }//GEN-LAST:event_jButtonResetActionPerformed

    private void jButtonRozrzucActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRozrzucActionPerformed
        clearCells(currentSpace);
        int procenty = jSlider1.getValue();
        for (int i = 1; i<width-1; i++)
            for (int j = 1; j< height-1; j++)
                if (Math.random()*100 < procenty) {
                        currentSpace[i][j].condition = true;
                }
        
        paintAgain();             
    }//GEN-LAST:event_jButtonRozrzucActionPerformed

    private void jButtonPeriodicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPeriodicActionPerformed
        periodic =! periodic;
        if (periodic) 
            jButtonPeriodic.setText("Periodyczność on");
        else
            jButtonPeriodic.setText("Periodyczność off");
    }//GEN-LAST:event_jButtonPeriodicActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Game.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Game.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Game.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Game.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Game().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonPeriodic;
    private javax.swing.JButton jButtonReset;
    private javax.swing.JButton jButtonRozrzuc;
    private javax.swing.JButton jButtonStart;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelBoard;
    private javax.swing.JPanel jPanelPropeties;
    private javax.swing.JSlider jSlider1;
    // End of variables declaration//GEN-END:variables
}
